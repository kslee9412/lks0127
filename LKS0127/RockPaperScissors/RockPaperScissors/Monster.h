#pragma once
#include "Character.h"
class Monster :
	protected Character
{
	char _monster;
	int _attack;
public:
	Monster();
	virtual ~Monster();

	void CheckHit();
	void InvadeMonster();
	void ShowStat();
	bool Attack();
};

