﻿#include "Character.h"
#include "Monster.h"

int main()
{
	Character Character;
	Monster Monster;

	Monster.InvadeMonster();
	Monster.ShowStat();
	Character.SelectInput();
	Monster.Attack();
	Monster.CheckHit();
	Monster.ShowStat();
}
