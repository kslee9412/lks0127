#pragma once
#include <iostream>
class Character
{
protected:
	int _hp;
	int _lv;
	char _name[16];
	bool _StillLive;
	bool _Hit;
	int _rock = 1;
	int _paper = 2;
	int _scissors = 3;
	int _input = 0;

public:
	Character();
	virtual ~Character();

	bool CheckLive();
	int SelectInput();
};