#pragma once
#include <iostream>
#include <string.h>
#include <conio.h>
class CCounter
{
private:
	int _counter = 0;
	char _count[1024] = {};
	bool isend = true;
	char _enter[2] = {'\n'};
	char _end[4] = { "end" };
public:
	CCounter();
	virtual ~CCounter();

	void SetCounter();
	void Counting();
	void ShowCounter();
};

