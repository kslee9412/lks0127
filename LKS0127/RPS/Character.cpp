#include "Character.h"
#include "Player.h"
#include "MOnster.h"
#include <iostream>

Character::Character()
	: _rock(0)
	, _paper(0)
	, _scissors(0)
{
}

Character::~Character()
{
}

void Character::BattlePage()
{
	if (_playerselect == _rock && _monsterselect == _paper)
	{
		_PlayerHit = true;
		Player GetDamge();
	}
	else if (_playerselect == _paper && _monsterselect == _scissors)
	{
		_PlayerHit = true;

		Player GetDamge();
	}
	else if (_playerselect == _scissors && _monsterselect == _rock)
	{
		_PlayerHit = true;

		Player GetDamge();
	}
	else if (_playerselect == _rock && _monsterselect == _scissors)
	{
		_MonsterHit = true;
		Monster GetDamage();
	}
	else if (_playerselect == _scissors && _monsterselect == _paper)
	{
		_MonsterHit = true;
		Monster GetDamage();
	}
	else if (_playerselect == _paper && _monsterselect == _rock)
	{
		_MonsterHit = true;
		Monster GetDamage();
	}
	else
	{
		_PlayerHit, _MonsterHit = false;
		Monster GetDamage();
	}
}

