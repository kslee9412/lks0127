#pragma once
#include "Character.h"
class Monster :
	protected Character
{
protected:
	int _maxhp = 30;
	int _hp = 30;

public:

	int MonsterAttack();
	void GetDamage();
};

