#pragma once
#include "Character.h"

class Player :
	protected Character
{
		int _maxhp = 50;
		int _maxmp = 50;
		int _hp = 50;
		int _mp = 50;
		int _skill_heal = 1;
		int _skill_plusdamage = 2;
		int _input;

	public:
		void ShowStat();
		int SelectBattle();
		void GetDamge();

		Player();
		virtual ~Player();
};