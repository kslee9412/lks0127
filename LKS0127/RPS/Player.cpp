#include "Player.h"
#include <iostream>

Player::Player()
{
}

Player::~Player()
{
}

void Player::ShowStat()
{
	std::cout << "플레이어" << std::endl;
	std::cout << "HP : " << _hp << std::endl;
	std::cout << "MP : " << _mp << std::endl;
	std::cout << "기술1. 사기치기 : HP를 10 회복" << std::endl;
	std::cout << "기술2. 죽창전 : 1회 한정 서로 받는 데미지 10 증가" << std::endl;
}

int Player::SelectBattle()
{
	std::cout << std::endl;
	std::cout << "1. 스킬1 사용, 2.스킬2 사용, 3.배틀페이즈"<<std::endl;

	bool ChoiceRight = false;

	while (ChoiceRight != true)
	{
		std::cout << "선택 : ";
		std::cin >> _input;
		switch (_input)
		{
		case 1:
			if (_hp == _maxhp)
			{
				std::cout << "이미 최대체력이다."<<std::endl;
				ChoiceRight = true;
				break;
			}
			else if (_hp < _maxhp)
			{
				std::cout << "밑장빼기는 기본이지.." << std::endl;
				std::cout << "HP 10 회복" << std::endl;
				_hp += 10;
				_mp -= 10;
				break;
			}
			else if (_mp < 10)
			{
				std::cout << "마나가 없다" << std::endl;
				ChoiceRight = true;
				break;
			}

		case 2:
			if (_skillcount = false)
			{
				std::cout << "공평하게 너도 한방, 나도 한방." << std::endl;
				_mp -= 50;
				_skillcount = true;
				ChoiceRight = true;
				break;
			}
			else
			{
				std::cout << "이미 죽창은 들었다." << std::endl;
				ChoiceRight = true;
				break;
			}

		case 3:
			ChoiceRight = true;
			break;

		default:
			std::cout << "잘못 선택했다.." << std::endl;
		}
	}

	while (1)
	{
		std::cout << "1. 가위, 2. 바위, 3. 보" << std::endl;
		std::cout << "선택 : ";
		std::cin >> _playerselect;

		if (_playerselect == 1)
		{
			_playerselect = _scissors;
			return _playerselect;
			_mp += 5;
			break;
		}
		else if (_playerselect == 2)
		{
			_playerselect = _rock;
			return _playerselect;
			_mp += 5;
			break;
		}
		else if (_playerselect == 3)
		{
			_playerselect = _paper;
			return _playerselect;
			_mp += 5;
			break;
		}
		else
		{
			std::cout << "잘못 선택했다.";
		}
	}
}

void Player::GetDamge()
{
	if (_PlayerHit == true)
	{
		std::cout << "패배" << std::endl;
		if (_skillcount = true)
		{
			_damage += 10;
			_hp -= _damage;
			_skillcount = false;
			_damage -= 10;
		}
		else
		{
			_hp -= _damage;
		}

	}
	else if (_hp == 0)
	{
		_StillLive = false;
		std::cout << "You Died" << std::endl;
		_GameisEnd = true;
	}
}
