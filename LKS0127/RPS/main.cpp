﻿#include "System.h"
#include "Character.h"
#include "Player.h"
#include "Monster.h"
#include <iostream>

int main()
{
	System System;
	Player Player;
	Monster Monster;
	Character Character;

	System.SystemPrint();
	Player.ShowStat();
	while (! Character.isGameEnd())
	{
		Player.SelectBattle();
		Monster.MonsterAttack();
		Character.BattlePage();
		Player.GetDamge();
		Monster.GetDamage();
	}
}