#pragma once

class Character
{
protected:
	int _hp;
	int _mp;
	int _maxhp;
	int _maxmp;
	int _playerselect;
	int _monsterselect;
	bool _skillcount = false;
	int _damage = 10;
	bool _PlayerHit = false;
	bool _MonsterHit = false;
	bool _GameisEnd = false;

	const int _rock;
	const int _paper;
	const int _scissors;

	bool _StillLive = true;

public:
	Character();
	Character(int rock, int paper, int scissors);
	virtual ~Character();

	void BattlePage();

	bool isGameEnd() {
		return this->_GameisEnd;
	}
};

