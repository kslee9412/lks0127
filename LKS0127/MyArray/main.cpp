﻿#include "Point.h"
#include <iostream>

int main()
{
	Point p(10, 20);
	Point s(30, 40);

	p.Show();
	s.Show();

	++p;

	p.Show();
	p++;
	p.Show();

	system("pause");

	return 0;
}
