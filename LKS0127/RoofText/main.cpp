﻿#include "RoofText.h"
#include <stdlib.h>
/*
2.회문
앞으로 읽으나 뒤로 읽으나 차이가 없는 단어
예를 들면 'level', 'bob'과 같은 단어들
입력한 단어가 회문인지 아닌지 출력해 주는 프로그램
*/

/*
구조
1. text를 입력
2. 입력받은 text의 시작점과 종료점 비교
3. 시작점과 종료점을 계속 비교
4. 전부 동일 할 경우 회문 출력 아닐경우 비회문 출력
*/
int main()
{
	CRoofText RoofText;
	
	RoofText.InputText();
	RoofText.OutputText();

	system("pause");
}