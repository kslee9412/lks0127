﻿#pragma once
class CRoofText
{
public:
	void InputText();
	void OutputText();

public:
	CRoofText();
	virtual ~CRoofText();

private:
	char _text[256] = { 0 }; //받아올 텍스트
	int _maximumtext = 0; //텍스트 길이 비교용(sizeof값 저장용)
	char _cmptext[256] = { 0 };
	bool isrooftext = true;
};