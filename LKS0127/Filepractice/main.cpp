﻿#include <iostream>
#include "gugudan.h"

/*
단수를 입력하면 그에 맞는 단을 저장하는 파일함수를 만들기
ex) -> input 2
2 x 1 = 2
2 x 2 = 4
.
.
.
*/

int main()
{
	Cgugudan gugudan;
	gugudan.InputNumber();
	gugudan.SaveFile();
}