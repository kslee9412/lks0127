﻿#pragma once

class CGetPoint
{
private:
	int _x, _y;
	int _key;
public:
	CGetPoint();
	virtual ~CGetPoint();

public:
	void SetPosition();
	void GotoXY(int _x, int _y);
	void DeleteXY(int _x, int _y);
	void MovePosition();
};

