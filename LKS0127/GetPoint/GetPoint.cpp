﻿#include "GetPoint.h"
#include <iostream>
#include <windows.h>
#include <conio.h>

CGetPoint::CGetPoint()
{
}


CGetPoint::~CGetPoint()
{
}

void CGetPoint::SetPosition()
{
	_x = 10;
	_y = 10;
	GotoXY(_x, _y);
}

void CGetPoint::GotoXY(int _x, int _y)
{
	COORD pos = { _x, _y };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);

	std::cout << '*' << std::endl;
}

void CGetPoint::DeleteXY(int _x, int _y)
{
	COORD pos = {_x,_y };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);

	std::cout << ' ' << std::endl;
}


void CGetPoint::MovePosition()
{
	while (1)
	{
		if (_kbhit())
		{
			_key = getch();
			if (_key == 224 || _key == 0)
			{
				_key = _getch();
				switch (_key)
				{
				case 72: //방향키 위 입력
			
					std::cout << ' ' << std::endl;
					_y++;

					COORD pos = { _x,_y };
					SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
					std::cout << '*' << std::endl;

				case 75: //방향키 좌 입력

					std::cout << ' ' << std::endl;
					_x--;
					COORD pos = { _x,_y };
					SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
					std::cout << '*' << std::endl;

				case 77: //방향키 우 입력

					std::cout << ' ' << std::endl;
					_x++;

					COORD pos = { _x,_y };
					SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
					std::cout << '*' << std::endl;

				case 80: //방향키 하 입력

					std::cout << ' ' << std::endl;
					_y--;

					COORD pos = { _x,_y };
					SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
					std::cout << '*' << std::endl;
				}
			}
		}
	}
}
