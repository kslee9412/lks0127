﻿#pragma once
#include <iostream>

class COrderSystem
{
private:
	int _a = 300; // 천하장사
	int _b = 500; // 박카스
	int _c = 1000; // 포카칩
	int _acount = 0;
	int _bcount = 0;
	int _ccount = 0;
	int money = 10000;
	int dividemoney[3] = { 4000, 3000, 3000 };

public:
	COrderSystem();
	virtual ~COrderSystem();

	void ShowCase();
	void DivideMoney();
	void ShowOrder();
};

