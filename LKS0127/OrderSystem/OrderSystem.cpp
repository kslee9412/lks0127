﻿#include "OrderSystem.h"


COrderSystem::COrderSystem()
{
}


COrderSystem::~COrderSystem()
{
}

void COrderSystem::ShowCase()
{
	std::cout << "천하장사 : 300원 | 박카스 : 500원 | 포카칩 : 1000원 " << std::endl;
	std::cout << "현재 발주 가능 금액 : " << money << std::endl;
}

void COrderSystem::DivideMoney()
{	// 천하장사 발주 카운트
	_acount = dividemoney[0] / _a;
	dividemoney[0] = dividemoney[0] - (_a * _acount);
	
	// 박카스 발주 카운트
	_bcount = dividemoney[1] / _b;
	dividemoney[1] = dividemoney[1] - (_b * _bcount);

	// 포카칩 발주 카운트
	_ccount = dividemoney[2] / _c;
	dividemoney[2] = dividemoney[2] - (_c * _ccount);

	// 잔돈 통합
	money = dividemoney[0] + dividemoney[1] + dividemoney[2];

	if (money >= 1000)
	{
		int temp = 0;
		temp = money / _a;
		_acount += temp;
		money -= _a * temp;
	}
	else if (money >= 500)
	{
		int temp = 0;
		temp = money / _b;
		_bcount += temp;
		money -= _b * temp;
	}
	else if (money >= 300)
	{
		int temp = 0;
		temp = money / _c;
		_ccount += temp;
		money -= _c * temp;
	}
}

void COrderSystem::ShowOrder()
{
	std::cout << "발주 계산" << std::endl;
	std::cout << "천하장사 : " << _acount << "개" << std::endl;
	std::cout << "박카스 : " << _bcount << "개" << std::endl;
	std::cout << "포카칩 : " << _ccount << "개" << std::endl;
	std::cout << "잔돈 : " << money << "원" << std::endl;
}
