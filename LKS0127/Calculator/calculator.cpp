#include <iostream>

/*
추가사항 ; 사칙연산을 할 수 있는 기능 추가
단 함수호출은 한번

ex
Plus
Multiply
Minus
Divide

int(*TestFunc)() = NULL;

if(input[1] = +)

TestFunc = Plus;

TestFunc()
*/
using namespace std;

int calculatecount = 0;
int inputnumber1 = 0;
int inputnumber2 = 0;
int result;

bool isEnd = false;

int Calculate(int inputnumber1, int inpunumber2); //계산 함수
int GetNumber(int inputnumber); // 값 입력 함수
void PrintOut(int result, int calculatecount); // 출력함수
bool JudgeEnd(bool isEnd, int result); //종료여부 판단 함수

int main()
{
	while (isEnd != true)
	{
		inputnumber1 = GetNumber(inputnumber1);
		inputnumber2 = GetNumber(inputnumber2);
		result = Calculate(inputnumber1, inputnumber2);
		isEnd = JudgeEnd(isEnd, result); //종료여부 판단
		PrintOut(result, calculatecount);
		++calculatecount; //계산횟수 증가
	}
}

int GetNumber(int inputnumber)
{
	cin >> inputnumber;

	return inputnumber;
}

int Calculate(int inputnumber1, int inputnumber2)
{
	return inputnumber1 * inputnumber2;
}

void PrintOut(int result, int calculatecount)
{
	if(result != 0)
	{
		cout << result << endl;
	}
	else
	{
		cout << "계산 횟수 : " << calculatecount << endl;
	}
}

bool JudgeEnd(bool isEnd, int result)
{
	if (result != 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}