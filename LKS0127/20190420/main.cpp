#include <iostream>
#include "Whatnumber.h"
#include <stdlib.h>
/*
1. 홀짝배열
크기가 10인 배열 선언하고 10개 정수 입력
홀수와 짝수 구분 지어서 출력하는 프로그램
*/

int main()
{
	CWhatnumber Whatnumber;

	Whatnumber.InputNumber();
	Whatnumber.CompareNumber();
	Whatnumber.OutputNumber();

	system("pause");
}