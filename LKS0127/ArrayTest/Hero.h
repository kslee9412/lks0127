﻿#pragma once
#include <iostream>

class Hero
{
private:
	//기본 공통사항
	//hp, mp, 점프 가능 높이, 이름, 힘, 지능, 민첩, 방어력, 치명타 확률, 생존여부, 무기
	int _hp;
	int _mp;
	int _jumpheight;
	char _name;
	int _str;
	int _int;
	int _dex;
	int _def;
	int _critical;
	bool StillLive;
	int _weapon;
	int _damge;
public:
	Hero();

	int Damage();
	void CheckLive();
	virtual ~Hero();
};

