#pragma once
class CSavingCan
{
public:
	void GetNumber(int _inputnum);
	void SumNumber();
	void IsEnd();
public:
	CSavingCan();
	virtual ~CSavingCan();
private:
	int _sum;
	int* _inputnum;
	bool _isEnd;
};